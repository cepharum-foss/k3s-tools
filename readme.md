# k3s cluster

This project is providing documentation and convenient tools for installing and maintaining a cluster based on k3s.

## Background

This tutorial is an attempt to learn k8s by doing. For this, we have set up a project containing multiple vServers at Hetzner Cloud. Other providers will definitely work as well, though some aspects might need to be adjusted or require extra steps to fix deviations from what is provided by Hetzner Cloud.

* For a start, all nodes are created as small as possible.

* All nodes reside in one data center. Probably, having a cluster spanning multiple data centers of same provider might be an option, though. We will look into that later.

* There are three master nodes. 

  Three nodes are fine for accepting one node e.g. to fail or go into maintenance. You could create five as well so that two of them can fail or be maintained. 
  
  **The number of master nodes must be odd** so the cluster can find a majority to decide on every change to the state of the cluster. More than 5 nodes is possible, but there is no benefit unless your data center tends to have regularly failing nodes. However, get a different data center in that case.

* There are three worker nodes. 

  These are the ones expected to be scaled up when load is growing. You can also add more worker nodes to the cluster now or later. It's fine to have an even number of worker nodes, too. Starting with one worker node is another viable option here, though this tutorial is going to set up a storage distributed to all worker nodes which relies on a similar approach for finding consensus through decisions made by a majority of nodes, so three worker nodes are recommended here.

* There are two load balancers. These aren't actual part of the cluster but reside in the same private network solely listening to all incoming requests of users.

* Oh, yes, there is a private network linking all those servers behind the scenes. The network is 10.0.0.0/16 with several sub-networks for separating master nodes (10.0.1.0/24) from worker nodes (10.0.2.0/24) from load balancers (10.0.200.0/24).

* And there is a firewall on provider side which is opening port 22/TCP for SSH on all servers, only. Ports 80/TCP and 443/TCP are open on the load balancers, only.

* As of 2022, IPv4 is required on every node for pulling k3s installer or pulling pod images does not work without it. That's a bummer in times of trying to pushing IPv6 getting its break-through moment eventually.

* All nodes run Ubuntu 22.04 LTS.

Here is a diagram for illustration:

![Diagram of resulting cluster structure](assets/k3s%20cluster.png)

## Get started

Read the [tutorial for setting up the cluster](docs/01-install-nodes.md).
