# Installing cluster with k3s-install.sh

The script [k3s-install.sh](../tools/k3s-install.sh) is a wrapper around k3s to simplify the basic steps of turning a loose set of servers into nodes of a k3s cluster. Yes, k3s installation is pretty simple, already. This script is a biased helper to set up nodes with less typing.

## Common preparation for all nodes

- _Optional, but recommended:_ attach either node to an internal/private network.

- _Optional, but recommended:_ set up firewall for SSH port, only, then assign it to every manager/worker node.

- Log into either node via SSH.

- Set alias `k` for `kubectl`, set `nano` as default for editing resources in k8s, install all available OS updates and reboot the node once eventually:
  
  ```bash
  echo -e "alias k=kubectl\nexport KUBE_EDITOR=nano" >>~/.profile && apt update && apt upgrade -y && reboot
  ```

- **On master nodes**, clone this repository:
  
  ```bash
  git clone https://gitlab.com/cepharum-foss/k3s-cluster.git
  alias k3s-install.sh=$(pwd)/k3s-cluster/tools/k3s-install.sh
  ```

  Set alias is temporary for you are going to use this command once right after this, only. If you reboot prior to using it, just redefine that alias.

- **On worker nodes**, its sufficient to fetch the installer script, only:
  
  ```bash
  curl -O https://gitlab.com/cepharum-foss/k3s-cluster/-/raw/main/tools/k3s-install.sh
  chmod +x k3s-install.sh
  alias k3s-install.sh=$(pwd)/k3s-install.sh
  ```

  Set alias is temporary for you are going to use this command once right after this, only. If you reboot prior to using it, just redefine that alias.

## Master nodes

- Create three master nodes and prepare them as described above.

- On first master node invoke installer:
    
  ```bash
  k3s-install.sh <node-ip>
  ```

  Use node's IP in internal/private network here.

  After success, final output includes the token to use next for joining nodes.

- On all other master nodes invoke installer now:

  ```
  k3s-install.sh <node-ip> <join-ip> <token>
  ```

  Arguments are:
  
  1. either node's IP in internal/private network
  2. the IP of first node created above
  3. token displayed on installing first node - use this command on first node to fetch token again:

     ```bash
     cat /var/lib/rancher/k3s/server/node-token
     ```
  
  ### Install different version
  
  Stable release is used by default. Pick a different channel for other versions on first master node:

  ```
  INSTALL_K3S_CHANNEL=v1.24 k3s-install.sh <node-ip>
  ```

  and on additional master nodes:

  ```
  INSTALL_K3S_CHANNEL=v1.24 k3s-install.sh <node-ip> <join-ip> <token>
  ```

  > As of early December 2022, using v1.24 seems to be required in case you want to follow instructions on setting up Traefik for ingress routing, later.

## Worker nodes

- Create any number of worker nodes you like. You should start with at least three worker nodes, though, for setting up persistent storage manager Longhorn in a later step.

- Invoke installer:

  ```
  k3s-install.sh <node-ip> <join-ip> <token> agent
  ```

  Arguments are:
  
  1. either node's IP in internal/private network
  2. the IP of any master node created before
  3. token displayed on installing first node - use this command on first node to fetch token again:

     ```bash
     cat /var/lib/rancher/k3s/server/node-token
     ```
  4. the keyword `agent` (provided literally) is requesting to join cluster as agent/worker
  
  Stable release is used by default. Pick a different channel for other versions:

  ```
  INSTALL_K3S_CHANNEL=v1.24 k3s-install.sh <node-ip> <join-ip> <token> agent
  ```

## Next Step

You have a basically running cluster, now.

* [Tweak your cluster](02-post-installation.md)
