# Install Longhorn

Longhorn implements a replicated persistent storage for pods that's automatically distributed over nodes of the cluster. Setting it up next is required for the ingress router to store its TLS certificates.

## Preparation

- Make sure there is sufficient disk space on either worker node for it will be managed by Longhorn.

- _Optional, but strongly recommended:_ attach resizable volumes with XFS format to either node and mount them under `/var/lib/longhorn`.

  In case of Hetzner Cloud, using such volumes simplifies process of scaling up your storage without any downtime. For either worker node:
  
  * change the size of the attached volume in Hetzner Cloud console.
  
  * log into either node via SSH and run:

    ```bash
    xfs_growfs /var/lib/longhorn
    ```

  After a few moments, longhorn will detect the new size of volume and adapt its offer for available storage accordingly.

  > You don't have to scale volumes attach to either node to same size. But it's probably for the best to do so as imbalance might diminish Longhorn's ability to always provide persistent volumes on claiming pod's current host.

- Connect to master node as before, but make sure your local port 9000 is forwarded to the master node via SSH tunnel, e.g. by using

  ```bash
  ssh -L 9000:127.0.0.1:9000 user@master-node
  ```

## Install Longhorn

- Just follow https://longhorn.io/docs/latest/deploy/install/install-with-helm/.

  All commands to be executed in order are:

  ```bash
  helm repo add longhorn https://charts.longhorn.io
  helm repo update
  k create ns longhorn-system
  helm install longhorn longhorn/longhorn --namespace longhorn-system
  ```

- Check all pods are running:

  ```bash
  k -n longhorn-system get pod
  ```

## Access dashboard

- Forward port to access Longhorn's dashboard via SSH tunnel on port 9000:

  ```bash
  k port-forward -n longhorn-system svc/longhorn-frontend 9000:80
  ```

  Keep the command running while opening https://127.0.0.1:9000 in browser for acccessing the dashboard.

## Suggestions and information

- Disable master nodes being scheduled. Open **Node** tab, then use context menu per listed master node to **Edit node and disk** and **disable** the **Scheduling** option before saving.

- A storage class named `longhorn` has been set up for you already. It is available for dynamically binding persistent volumes (PVs) to pods through persistent volume claims (PVCs). We are going to use this on setting up ingress router next.

Stop the port forwarding command when you are done using the UI.

## Next step

Now that your cluster is capable of safely providing persistent storage, it's time to set up an ingress router in preparation for running applications becoming available to the public.

* [Installing Traefik](04-ingress-traefik.md)
