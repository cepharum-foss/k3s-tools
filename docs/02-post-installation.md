# Post-installation steps

Following steps commonly assume to log into particular master node via SSH.

## Cordon master ndoes

Cordon all master nodes to prevent any work load of cluster being scheduled there:

- Run this command:

  ```bash
  k cordon $(k get nodes | grep master | awk '{print$1}')
  ```

## Write kube configuration

Tools like `helm` read k8s configuration. This command is exposing it in a commonly expected file:

- Write config to file and limit access on it:

  ```bash
  k config view --raw >~/.kube/config
  chmod go-rwx ~/.kube/config
  ```

## Install helm

- https://helm.sh/docs/intro/install/#from-apt-debianubuntu

  ```
  curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
  sudo apt-get install apt-transport-https --yes
  echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
  sudo apt-get update
  sudo apt-get install helm
  ```

## Next step

* [Persistent storage](03-longhorn.md)
