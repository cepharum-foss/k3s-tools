# Install Traefik

An ingress is forwarding incoming requests to services/pods of cluster based on rules. Traefik is an ingress. This setup is meant to work with [load balancers set up separately](05-loadbalancer.md).

This setup is inspired by:

* https://www.asykim.com/blog/deep-dive-into-kubernetes-external-traffic-policies
* https://github.com/traefik/traefik-helm-chart/issues/404

SSH into same master node where `helm` has been installed before.

- Install helm repo of Traefik:

  ```bash
  helm repo add traefik https://traefik.github.io/charts
  helm repo update
  ```

- Create a custom namespace for Traefik:

  ```bash
  k create ns traefik
  ```

- Create custom configuration **traefik.yaml** e.g. with following content:

  ```yaml
  # set up k8s networking and Traefik endpoints
  service:
    type: NodePort
    spec:
      externalTrafficPolicy: Local
  ports:
    web:
      nodePort: 32080
      proxyProtocol:
        trustedIPs:
          - 10.0.200.1/24
    websecure:
      nodePort: 32443
      proxyProtocol:
        trustedIPs:
          - 10.0.200.1/24
      tls:
        certResolver: letsencrypt
  
  # claim persistent volume for storing TLS certs
  persistence:
    enabled: true
    storageClass: longhorn
  
  # enable LetsEncrypt for auto-fetching TLS certs
  certResolvers:
    letsencrypt:
      caServer: https://acme-staging-v02.api.letsencrypt.org/directory
      storage: /data/acme.json
      email: ssladmin@example.com
      httpChallenge:
        entryPoint: "web"

  # fix issue with file permissions of acme.json file on restart
  deployment:
    initContainers:
      - name: fix-file-permissions
        image: busybox
        command: ["sh", "-c", "chown -Rf 65532:65532 /data/* && chmod -Rf go-rwx /data/*"]
        volumeMounts:
          - name: data
            mountPath: /data

  # extend logging for debugging - remove when done
  logs:
    general: DEBUG
    access:
      enabled: true
  ```

  > Make sure proper mail address is provided in `certResolvers` block.

  - First block is basically setting up Traefik to listen for incoming requests solely on node currently running Traefik pod on ports 32080 and 32443. Load balancers will be set up later to forward incoming traffic there.

  - Second block is enabling Traefik to claim for persistent volume from Longhorn which has been set up before.

  - Third block is enabling automatic management of TLS certificates using LetsEncrypt.

    > Using staging server is recommended while setting up things. If everything is working as desired, you should remove the `caServer` line and upgrade Traefik.

  - Fourth block is invoking a custom command on restart of Traefik to fix file permissions on certificate storage.

  - Final block is illustrating some options suitable for debugging issues with Traefik.

  The configuration is merged with [default values](https://github.com/traefik/traefik-helm-chart/blob/master/traefik/values.yaml).

- Try installation:

  ```bash
  helm install -n traefik --atomic -f traefik.yaml traefik traefik/traefik --dry-run
  ```

  This might fail due to k3s having installed (parts of) Traefik already. Just clean up.

  - Uninstall any release of k3s:

    ```bash
    helm uninstall -n kube-system traefik traefik-crd
    ```

  - _to be assessed:_ Remove the charts to prevent their scheduled installation by accident:

    ```bash
    k delete HelmChart -n kube-system traefik traefik-crd
    ``` 

  - Remove any leftover custom resource definition related to Traefik:

    ```bash
    k delete crd $(k get crd -A | grep traefik.containo.us | awk '{print$1}')
    ```

  After that, try installing Traefik as given before again.
  
  If that test succeeds, install Traefik eventually:

  ```bash
  helm install -n traefik --atomic -f traefik.yaml traefik traefik/traefik
  ```

  __not working:__ After that, you can upgrade installed release e.g. after adjusting traefik.yaml file by running:

  ```bash
  helm upgrade -n traefik --atomic -f traefik.yaml traefik traefik/traefik
  ```

  > When upgrading, Traefik can be waiting infinitely for the pod to be replaced. Uninstalling, then re-installing is a working solution:
  >
  > ```bash
  > helm uninstall -n traefik traefik
  > helm install -n traefik --atomic -f traefik.yaml traefik traefik/traefik
  > ```

- Forward Traefik dashboard's port via SSH tunnel to port 9000:

  ```bash
  k port-forward -n traefik $(k get pods -n traefik -l app.kubernetes.io/name=traefik -o name) 9000:9000
  ```

  Keep the command running while opening http://127.0.0.1:9000/dashboard/ in a browser. Check if all routers, services and middlewares of HTTP endpoint are working properly.
  
  Stop the command when done.

# Configure Traefik

## Expose dashboard with authentication

Install `htpasswd` for creating properly encoded authentication secrets:

```bash
apt install -y apache2-utils
```

Exposing user interfaces with limited access requires quite complex object definition files. Thus, instead of showing content here, download [router.yaml](../definitions/router.yaml) file to your master node and adjust it:

1. Replace host name **router.example.com** with a host name you control. Make sure DNS is referring it to all your load balancers.

2. Create a record for every user granted access to the dashboard using:

   ```bash
   htpasswd -n <username> | base64
   ```

   Join all records with a comma, then base64-encode the resulting single-line string and inject it in second block of **router.yaml** file.

Eventually apply it to your cluster:

```bash
k apply -f router.yaml
```

The file consists of multiple constituents:

1. A middleware is created to apply a custom set of authentications.

2. Next, that custom set of authentications is created.

3. Additional middleware is created to basically deal with multiple redirections:

   - Redirect any request via HTTP to same URL via HTTPS.
   - Redirect URLs lacking prefix or required slashes as required for accessing the dashboard.

4. Then, a first IngressRoute is created applying first redirection to HTTP requests matching the hostname.

5. Finally, another IngressRoute is created to actually expose the dashboard over HTTPS.

Traefik dashboard will be available after that, if domain **router.example.com** is referring to your load balancers and after authenticating with username **admin** and password **nimda** or whatever secrets you've injected in second block of file above.

## More to expose

In the same way as before, use these linked examples, edit and apply them, to expose additional user interfaces that are already included with your cluster now:

- [Longhorn dashboard](../definitions/longhorn.yaml)

- [Traefik metrics](../definitions/metrics.yaml)

  Adjust list of peer IPs granted access in first block and host name to listen to in second block.

## Next Step

Your cluster is capable of internally routing incoming HTTP requests properly. But it lacks load balancers to receive requests from the public internet.

* [Load balancers](05-loadbalancer.md)
