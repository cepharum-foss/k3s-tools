# Load balancer

- Create two or more small servers.

  It's possible to have a single server here, as well. Having at least two load balancers isn't actually creating _high availability_, but having them in different places is a requirement for getting there in the first place. With multiple load balancers in place, you could have a DNS with very little TTL that's checking load balancers for being available and adjusting provided DNS records accordingly. Setting up such a DNS is beyond the scope of this tutorial.

- Attach to internal/private network shared with worker nodes of your cluster.

- Assign firewall enabling SSH, HTTP and HTTPS protocols, only.

- Prepare either server:

  ```bash
  apt update && apt upgrade -y && apt install -y haproxy && reboot
  ```

- Edit **/etc/haproxy/haproxy.cfg** and append this:

  ```
  peers k1-lb
      # list internal/private IPs of all load balancers
      peer k1-lb1 10.0.200.1:1024
      peer k1-lb2 10.0.200.2:1024
  
  frontend http_in
      bind :::80 v4v6
      default_backend http_out
  
  frontend https_in
      mode tcp
      bind :::443 v4v6
      default_backend https_out
  
  backend http_out
      mode http
      balance roundrobin
      # list internal/private IPs of all cluster worker nodes
      # port is NodePort of your cluster's ingress HTTP
      server k1-w1 10.0.2.1:32080 check send-proxy-v2
      server k1-w2 10.0.2.2:32080 check send-proxy-v2
      server k1-w3 10.0.2.3:32080 check send-proxy-v2
  
  backend https_out
      mode tcp
      balance roundrobin
      # list internal/private IPs of all cluster worker nodes
      # port is NodePort of your cluster's ingress HTTPS
      server k1-w1 10.0.2.1:32443 check send-proxy-v2
      server k1-w2 10.0.2.2:32443 check send-proxy-v2
      server k1-w3 10.0.2.3:32443 check send-proxy-v2
  ```

  This configuration is:

  - declaring all proxy servers created here as peers for interacting with each other.  

    > Either peer has a name and an IP address that you need to adjust to match your setup.
  
  - declaring frontends for listening on ports 80 and 443
  
  - declaring backends accordingly to forward incoming traffic on either frontend to all existing worker nodes.  

    > For either node there is name and an IP address that you need to adjust to match your setup.

  Keep listed peers and backend servers up-to-date, e.g. on adding more worker nodes or load balancers.

- Restart proxy:

  ```bash
  systemctl restart haproxy
  ```
