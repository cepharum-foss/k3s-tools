#!/bin/bash
#
# CLI script for draining a node incl.  authentication secrets for Traefik ingress routing
#
# (c) 2022 cepharum Berlin

set -eu

K="${KUBECTL:-kubectl}"

if [ $# -lt 1 ]; then
        echo "usage: $0 <node>" >&2
        exit 1
fi

NODE="$1"


"$K" cordon "$NODE"
"$K" drain "$NODE" \
        --ignore-daemonsets \
        --pod-selector='app!=csi-attacher,app!=csi-provisioner' \
        --delete-emptydir-data

cat <<EOT >&2

node $NODE has been drained. On worker nodes, revert with:

  $K uncordon $NODE

EOT
