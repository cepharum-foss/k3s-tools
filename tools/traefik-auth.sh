#!/bin/bash
#
# CLI script for managing authentication secrets for Traefik ingress routing
#
# (c) 2022 cepharum Berlin

set -eu

K="${KUBECTL:-kubectl}"


if [ "$#" -lt 1 ]; then
        # list available secrets
        "$K" get -A Secret | awk '{printf"%s/%s\n",$1,$2}'
        exit 0
fi


which jq &>/dev/null || sudo apt install jq -y


SELECTOR="${1}"
NS="${SELECTOR%%/*}"
NAME="${SELECTOR##*/}"

if [ "$NAME" = "$NS" ]; then NS=; else NS="-n $NS"; fi

if [ "$#" -lt 2 ]; then
        # list existing user names
        "$K" get $NS Secret "$NAME" -o json | jq -r .data.users | base64 -d | RS=, FS=: awk '{print$1}'
        exit 0
fi
