#!/bin/bash
# (c) 2022, cepharum GmbH

set -eu

[ "$#" -ge 1 ] || {
	cat <<EOT >&2
usage: $0 <node-ip> [ <cluster-join-ip> <join-token> [ agent ] ]

* Invoke with one argument to create new cluster.
* Invoke with three arguments to join a cluster as server/master.
* Invoke with four arguments to join a cluster as agent/worker.
EOT
	exit 1
}


NODE_IP="$1"

JOIN_SERVER_IP="${2:-}"
JOIN_SECRET="${3:-}"
MODE="${4:-server}"

if [ -n "${VERSION:-}" ]; then export INSTALL_K3S_CHANNEL="v${VERSION#v}"; fi


apt install wireguard pwgen -y

which kubectl &>/dev/null || rm -Rf /var/lib/rancher


IFACE="$(ip -o -br -4 addr show | awk -F '[ /]+' "\$3==\"$NODE_IP\"{print\$1}")"

if [ -n "$JOIN_SERVER_IP" -a -n "$JOIN_SECRET" ]; then
	case "${MODE,,}" in
		agent|worker)
			curl -sfL https://get.k3s.io | \
				K3S_TOKEN="$JOIN_SECRET" \
				K3S_AGENT_TOKEN="$(pwgen 32 1)" \
				sh -s - agent \
				--node-ip="$NODE_IP" \
				--flannel-iface "$IFACE" \
				--server "https://${JOIN_SERVER_IP}:6443"
			;;

		server|master|"")
			curl -sfL https://get.k3s.io | \
				K3S_TOKEN="$JOIN_SECRET" \
				sh -s - server \
				--node-ip="$NODE_IP" \
				--flannel-backend wireguard-native \
				--flannel-iface "$IFACE" \
				--server "https://${JOIN_SERVER_IP}:6443"
			;;

		*)
			echo "invalid mode for joining cluster: $MODE ... use one of agent, worker, server, master" >&2
			exit 1
			;;
	esac
else
	mkdir -p /etc/rancher/k3s
	cat <<EOT >/etc/rancher/k3s/config.yaml
disable: traefik
EOT

	curl -sfL https://get.k3s.io | \
		sh -s - server \
		--node-ip="$NODE_IP" \
		--flannel-backend wireguard-native \
		--flannel-iface "$IFACE" \
		--cluster-init

	cat <<EOT >&2


 - join token for nodes is:

     $(cat /var/lib/rancher/k3s/server/token)

 - use this IP for joining:

     $(ip -o -br -4 addr show | awk -F '[ /]+' '$3~/^10\./{print$3}' | head -1)


EOT
fi
